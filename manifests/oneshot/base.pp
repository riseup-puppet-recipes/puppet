# helper for puppet::oneshot, inheriting puppet::base
# so it can override Service['puppet']
class puppet::oneshot::base inherits puppet::base {

  Service['puppet'] {
    enable => false,
    ensure => stopped,
  }

}
