# run puppet agent once, at boot time
class puppet::oneshot (
  $config                           = '/etc/puppet/puppet.conf',
  $config_content                   = false,
  $cleanup_clientbucket             = false,
  $ensure_version                   = 'installed',
  $ensure_facter_version            = 'installed',
  $shorewall_puppetmaster           = false,
  $shorewall_puppetmaster_port      = '8140',
) {

  class{ 'puppet':
    config                      => $config,
    config_content              => $config_content,
    cleanup_clientbucket        => $cleanup_clientbucket,
    ensure_version              => $ensure_version,
    ensure_facter_version       => $ensure_facter_version,
    shorewall_puppetmaster      => $shorewall_puppetmaster,
    shorewall_puppetmaster_port => $shorewall_puppetmaster_port,
  }

  include puppet::oneshot::base
  include puppet::oneshot::linux

  file { '/etc/systemd/system/puppet-oneshot.service':
    owner   => root,
    group   => root,
    mode    => '0644',
    content => '[Unit]
Description=Puppet agent (one-shot)

[Service]
Type=oneshot
ExecStart=/usr/bin/puppet agent --onetime --no-daemonize
RemainAfterExit=yes

[Install]
WantedBy=multi-user.target
',
  }

  service { 'puppet-oneshot':
    enable    => true,
    provider  => systemd,
    require   => [
      Package['puppet'],
      File['/etc/systemd/system/puppet-oneshot.service']
    ],
    subscribe => File['/etc/systemd/system/puppet-oneshot.service'],
  }

  exec { 'systemctl enable puppet-oneshot.service':
    creates => '/etc/systemd/system/multi-user.target.wants/puppet-oneshot.service',
  }
}
